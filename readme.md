# ReadMe

- Pour ce test nous attentons une page responcive intégrée avec Semantic UI ( https://semantic-ui.com/ )

- utilisez le language twig pour setter des variables en haut de page et les reutilisées dans le corps afin d'épurer votre code HTML et faciliter une integration en template symfony.

- n'hésitez pas à utiliser des includes twig ou même une arborescence extends pour partitionner votre code.

- il n'y a pas de questions bêtes (je suis disponible sur technique@fmcproduction.com)

Bon code !